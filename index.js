/**
 * Created by lucky on 02.11.16.
 */

/**
 *
 * @returns {function(*): *}
 */
function Zero() {
    return base => base;
}


/**
 *
 * @param {function(function)} func
 * @returns {function(*=)}
 * @constructor
 */
function Succ(func) {
    return next => {
        return base => next(func(next)(base));
    }
}



const zero = Zero;

const two = Succ(Succ(Zero));

console.log(two(x => x + 1)(0));

const four = Succ(Succ(two));

console.log(four(x => x + 1)(0));
